package app.invoice;

public record Invoice(Long id, String number) {
}
