package app.order;

public record Order(Long id, String orderNumber) {
}
